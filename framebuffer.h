#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H
#include "gtk/gtk.h"
#include <cstdint>

unsigned char *createFrameBuffer(unsigned int W, unsigned int H, unsigned int XPAD, unsigned int YPAD);
void frameBuffer_setWindowPosition(double cx, double cy, int zoomFactor);
void genPixBuf(unsigned char *pixbuff);
void frameBuffer_pan(int dx, int dy);
void frameBuffer_zoom(int dzoom);
double getWindowPositionX();
double getWindowPositionY();
unsigned char*getOutBuf();
void set_col_map(uint32_t *colmap);
void set_key_cols(int num_key_cols,double*x,uint32_t*rgb);
void rand_key_cols(int num_key_cols=16);
void toggle_hist_eq();
void toggle_histogram_stretch();
void toggle_show_transitions();
void toggle_color_gradient();
void inc_bailout();
void dec_bailout();
void save_state();
void load_state();
class frameBuffer
{
public:
    frameBuffer();
};

#endif // FRAMEBUFFER_H
