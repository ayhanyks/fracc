#include "stdlib.h"
#include "stdio.h"
#include <iostream>
#include <string.h>

using namespace std;
#include "framebuffer.h"
#include <png.h>
#include <errno.h>

typedef struct{
    string x;
    string y;
    string z;
    string w;
    string h;
    string f;
}fractal_input_params;


typedef struct{
    double x;
    double y;
    double z;

    int w;
    int h;
    int wpad;
    int hpad;
    string f;
    
    string ref;
    string command; //zoom in, zoom out, pan, color, etc
    string commandpar;
}fractal_params;

fractal_input_params finputpar;
fractal_params fpar={
    .x=0,
    .y=0,
    .z=1,
    .w=800,
    .h=480,
    .wpad=0,
    .hpad=0,
    .f="fimg.png"
};


unsigned char*pixbuf;



int save_data(){
    //
}

int write_png_file(){

    png_bytep*row_pointers=new png_bytep[fpar.h];

    
    string filename=fpar.ref+".png";
    cout<<"file name:"<<filename<<endl;

    FILE *fp = fopen(filename.data(), "wb");
    if(!fp) abort();

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png) abort();

    png_infop info = png_create_info_struct(png);
    if (!info) abort();

    if (setjmp(png_jmpbuf(png))) abort();

    png_init_io(png, fp);

    // Output is 8bit depth, RGBA format.
    png_set_IHDR(
        png,
        info,
        fpar.w, fpar.h,
        8,
        PNG_COLOR_TYPE_RGB,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT
    );
    png_write_info(png, info);

    // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
    // Use png_set_filler().
    //png_set_filler(png, 0, PNG_FILLER_AFTER);


    for(int y=0;y<fpar.h;y++)
    {
        row_pointers[y]=getOutBuf()+y*fpar.w*3;
    }
//IMGW, IMGH, 3*(IMGW+2*XPAD)
    if (!row_pointers) abort();

    cout<<"writing image"<<endl;
    png_write_image(png, row_pointers);
    cout<<"writing image done"<<endl;

    
    png_write_end(png, NULL);


    free(row_pointers);

    fclose(fp);

    png_destroy_write_struct(&png, &info);

    return 0;
}

int arg_double(int argnum, int argc, char**argv, double*argval){
    if(argnum>=argc)
    {
        return -1;
    }

    char * e;
    errno = 0;

    *argval = strtod(argv[argnum], &e);

    if (*e != '\0' ||  // error, we didn't consume the entire string
        errno != 0 )   // error, overflow or underflow
    {
        return -1;
    }
    return 0;
} 

int arg_longint(int argnum, int argc, char**argv, int*argval){
    if(argnum>=argc)
    {
        return -1;
    }

    char * e;
    errno = 0;

    *argval = strtol(argv[argnum],nullptr,10);

    if (errno != 0 )   // error, overflow or underflow
    {
        cout<<"can not convert" << argv[argnum] <<"to integer"<<endl;
        return -1;

    }
    return 0;
}

int arg_path(int argnum, int argc, char**argv, string*argval){
    if(argnum>=argc)
    {
        return -1;
    }

    char * e;
    errno = 0;

    *argval = argv[argnum];

    return 0;
}
int arg_str(int argnum, int argc, char**argv, string*argval){
    if(argnum>=argc)
    {
        return -1;
    }

    char * e;
    errno = 0;

    *argval = argv[argnum];

    return 0;
}

void parse_args(int    argc,
      char **argv){
    
    int a = 1;
    int e=0;
    while(a<argc)
    {
        e = -1;
        string arg=argv[a];

        if (arg=="-r"){
            e=arg_str(a+1,argc,argv,&fpar.ref);
            a+=2;
        }
        else if(arg=="-x"){
            e=arg_double(a+1,argc,argv,&fpar.x );
            a+=2;
        }
        else if(arg=="-y"){
            e=arg_double(a+1,argc,argv,&fpar.y );
            a+=2;
        }
        else if(arg=="-z"){
            e=arg_double(a+1,argc,argv,&fpar.z );
            a+=2;
        }
        else if(arg=="-h"){
            e=arg_longint(a+1,argc,argv,&fpar.h );
            a+=2;
        }
        else if(arg=="-w"){
            e=arg_longint(a+1,argc,argv,&fpar.w );
            a+=2;
        }
        else if(arg=="-f"){
            e=arg_path(a+1,argc,argv,&fpar.f );
            a+=2;
        }
        if(e){
            cout<<"can not parse arg "<<arg<<endl;
            abort();
        }

        
    }

}

int
main (int    argc,
      char **argv)
{

    //parameters
    //-x     string
    //-y     string
    //-z     zoom level 
    //-w     output size W
    //-h     output size H
    //-f     output file name (.png)

    cout << "You have entered " << argc
         << " arguments:" << "\n";
  
    for (int i = 0; i < argc; ++i)
        cout << argv[i] << "\n";

    printf("hello world..\n");

    parse_args(argc,argv);
   
    createFrameBuffer(fpar.w, fpar.h, fpar.wpad, fpar.hpad );

    frameBuffer_setWindowPosition(fpar.x,fpar.y,fpar.z);

    save_data();
    write_png_file();

    return 0;

}
