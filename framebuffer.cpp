#include "framebuffer.h"

#include "fpu.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#define NUMTHREADS 8
#define DEF_LEVELS 4096

// static guchar*pixbuff;
#define NUMBUFS 4

static uint32_t *outputBuf[NUMBUFS];
static unsigned char *PIXBUFF[NUMBUFS];
static uint32_t current_buf_id;

static WORKLOAD *Window;
static unsigned int IMGW;
static unsigned int IMGH;
static unsigned int IMGXPAD;
static unsigned int IMGYPAD;
static unsigned int out_buf_size;
static unsigned int pix_buf_size;
static void ppe(WORKLOAD *W);

// #define NUM_COLMAP 256
// static uint32_t colormapR[NUM_COLMAP];
// static uint32_t colormapG[NUM_COLMAP];
// static uint32_t colormapB[NUM_COLMAP];

unsigned char *createFrameBuffer(unsigned int W, unsigned int H, unsigned int XPAD, unsigned int YPAD)
{
    IMGW = W;
    IMGH = H;
    IMGXPAD = XPAD;
    IMGYPAD = YPAD;

    current_buf_id = 0;
    out_buf_size = (W + XPAD * 2) * (H + YPAD * 2) * sizeof(uint32_t);
    pix_buf_size = (W + XPAD * 2) * (H + YPAD * 2) * sizeof(unsigned char) * 3;
    for (int bid = 0; bid < NUMBUFS; bid++)
    {
        outputBuf[bid] = (uint32_t *)malloc(out_buf_size);
        PIXBUFF[bid] = (unsigned char *)malloc(pix_buf_size);
    }
    // pixbuff = (guchar*)malloc(W*H*sizeof(guchar)*3);

    Window = (WORKLOAD *)malloc(sizeof(WORKLOAD));

    Window->done = false;
    Window->H = IMGH + 2 * YPAD;
    Window->W = IMGW + 2 * XPAD;

    Window->xc = 0;
    Window->yc = 0;
    Window->zoom = 1;
    Window->xstart = 0;
    Window->xend = IMGW + 2 * XPAD;
    Window->ystart = 0;
    Window->yend = IMGH + 2 * YPAD;
    Window->outputBuf = outputBuf[current_buf_id];
    Window->ppeBuf = PIXBUFF[current_buf_id];
    Window->callback = ppe;
    Window->numlvl = DEF_LEVELS;
    Window->colormap = (uint32_t *)malloc(sizeof(uint32_t) * Window->numlvl);
    Window->hist_eq = false;
    Window->hist_stretch = false;
    Window->show_trans = false;
    Window->color_gradient=true;
    Window->histogram = new uint32_t[Window->numlvl];
    Window->bailout = 4;
    for (int i = 0; i < Window->numlvl; i++)
    {
        Window->histogram[i] = 0;
        Window->colormap[i] = (i << 16) | (i << 8) | i;
    }

    //startWorkload(Window);

    unsigned char *OUT;
    OUT = (unsigned char *)((size_t)PIXBUFF + YPAD * (W + 2 * XPAD) * 3 * sizeof(unsigned char) + XPAD * 3 * sizeof(unsigned char));
    return OUT;
}

unsigned char *getOutBuf()
{
    unsigned char *outb;

    outb = (unsigned char *)((size_t)PIXBUFF[current_buf_id] + IMGYPAD * (IMGW + 2 * IMGXPAD) * 3 * sizeof(unsigned char) + IMGXPAD * 3 * sizeof(unsigned char));
    ;

    return outb;
}

void ppe(WORKLOAD *W)
{
    // printf("callback 0x%08x\n",W);
    uint32_t x, y;
    uint32_t pbx;
    uint32_t pvx;
    uint32_t oval,oval_pre, oval_next;

    uint32_t *obuf = W->outputBuf;
    guchar *pbuf = (guchar *)W->ppeBuf;
    uint32_t WH = (W->W*W->H);
    uint32_t*T = new uint32_t[W->numlvl];
    double cdf = 0;
    int hist_min = -1;
    int hist_max = -1;

    for(x=0;x<W->numlvl;x++)
    {
        Window->histogram[x]=0;
    }
    uint32_t vxx=0;
    hist_min = obuf[0]&0xFFFF;
    hist_max = obuf[0]&0xFFFF;

    for (y = 0; y < W->H; y++)
    {
        for (x = 0; x < W->W; x++)
        {
            
            uint32_t itrnum = obuf[vxx++] & 0xFFFF;
            hist_min=hist_min>itrnum?itrnum:hist_min;
            hist_max=hist_max<itrnum?itrnum:hist_max;

            Window->histogram[itrnum]++;
            
        }
    }
    
    cdf = 0;

    for(x=0;x<W->numlvl;x++)
    {
        if(Window->hist_eq)
        { 
            cdf += 1.0*Window->histogram[x] / (WH);
            T[x] = (W->numlvl-1)*cdf;
        }
        else if(Window->hist_stretch)
        {
            T[x] = ((W->numlvl-1)*(x-hist_min))/(hist_max-hist_min);

        }
        else
        {
            T[x] = x;
        }
       // printf("x: %d cdf:%0.3f T:%d\n",x,cdf,T[x]);



    }

    for (y = 0; y < W->H; y++)
    {
        pbx = (W->W * y) * 3;

        pvx = (W->W * y);

        for (x = 0; x < W->W; x++)
        {
            
            uint32_t itrnum = obuf[pvx] & 0xFFFF;
            uint8_t itr_lvl = obuf[pvx] >> 16;

            uint8_t r,g,b,r0,g0,b0,r1,g1,b1,rm,gm,bm;
            int R,G,B;
            pvx ++;

            oval = T[itrnum];
            if (itrnum)
            {
                oval_pre = T[itrnum-1];
            }
            else{
                oval_pre = oval;
            }

            if(itrnum<(W->numlvl-1))
            {
                oval_next = T[itrnum+1];
            }
            else
            {
                oval_next = oval;
            }

            r = (Window->colormap[oval] & 0x0000FF);
            g = (Window->colormap[oval] & 0x00FF00)>> 8;
            b = (Window->colormap[oval] & 0xFF0000)>> 16;

            if(W->color_gradient)
            {


                r0 = (Window->colormap[oval_pre] & 0x0000FF);
                g0 = (Window->colormap[oval_pre] & 0x00FF00)>> 8;
                b0 = (Window->colormap[oval_pre] & 0xFF0000)>> 16;

                r1 = (Window->colormap[oval_next] & 0x0000FF);
                g1 = (Window->colormap[oval_next] & 0x00FF00)>> 8;
                b1 = (Window->colormap[oval_next] & 0xFF0000)>> 16;


                if(itr_lvl<128)
                {

                    rm = (r1+r)/2;
                    gm = (g1+g)/2;
                    bm = (b1+b)/2;


                    R = rm-((rm-r)*itr_lvl)/128;
                    G = gm-((gm-g)*itr_lvl)/128;
                    B = bm-((bm-b)*itr_lvl)/128;
                }
                else if(itr_lvl>128)
                {

                    rm = (r0+r)/2;
                    gm = (g0+g)/2;
                    bm = (b0+b)/2;

                    R = r-((r-rm)*(itr_lvl-128))/127;
                    G = g-((g-gm)*(itr_lvl-128))/127;
                    B = b-((b-bm)*(itr_lvl-128))/127;      
                }
                if(itr_lvl==128)
                {
                    R=r;G=g;B=b;
                }
                //if(y==400)
                //    printf("ov: %d r0:%d g0:%d b0:%d r1:%d b1:%d g1:%d :%d g:%d b:%d rm:%d gm:%d bm:%d R:%d G:%d B:%d il:%d \n",oval, r0,g0,b0,r1,g1,b1, r,g,b,rm,gm,bm,R,G,B,itr_lvl);
        }
        else{
            R=r;
            G=g;
            B=b;
        }

        if(W->show_trans)
        {
            R=itr_lvl;
            G=itr_lvl;
            B=itr_lvl;
        }

            pbuf[pbx++] = R;
            pbuf[pbx++] = G;
            pbuf[pbx++] = B;

            if(R<0 || G<0 || B<0)
            {
                printf("%d %d %d | %d %d %d | %d %d %d | %d %d %d\n",r,g,b,r0,g0,b0,r1,g1,b1,R,G,B);
                return;
            }
           
        }
    }

    delete T;
}
//smooth color transition:
//U...|......x....|.....L
//gradient map;
//zooming: gradient map 

void ppex(WORKLOAD *W)
{
    // printf("callback 0x%08x\n",W);
    uint32_t x, y;
    uint32_t pbx;
    uint32_t pvx;
    uint32_t oval;

    uint32_t *obuf = W->outputBuf;
    guchar *pbuf = (guchar *)W->ppeBuf;
    uint32_t WH = (W->W*W->H);
    uint32_t*T = new uint32_t[W->numlvl];
    double cdf = 0;
    int hist_min = -1;
    int hist_max = -1;

    for(x=0;x<W->numlvl;x++)
    {
        Window->histogram[x]=0;
    }
    uint32_t vxx=0;
    hist_min = obuf[0];
    hist_max = obuf[0];

  /*  
    cdf = 0;
    int*glinex=new int[W->W];
    for (y = 0; y < W->H; y++)
    {
        pbx = (W->W * y) * 3;
        pvx = (W->W * y);
        int gstart=0;
        int gstart_diff=0;
        int gend_diff=0;

        for (x = 0; x < W->W; x++)
        {
            uint32_t ov=obuf[pvx];

            

        }
    }
    */
    for (y = 0; y < W->H; y++)
    {
        pbx = (W->W * y) * 3;

        pvx = (W->W * y);

        for (x = 0; x < W->W; x++)
        {

            uint32_t ov=obuf[pvx];

            //find gradient//horizontal
            int gl=0,gr=0,gb=0,gu=0;

            uint32_t gxl;
            uint32_t gxr;
            uint32_t gyu;
            uint32_t gyb;

            for(gxl=x;gxl;gxl--)
            {
                if(obuf[pvx+gxl-x]!=ov)
                {
                    gl = obuf[pvx+gxl-x] - ov;
                    break;
                }
            }
            for(gxr=x;gxr<W->W;gxr++)
            {
                if(obuf[pvx+gxr-x]!=ov)
                {
                    gr =  ov - obuf[pvx+gxr-x];
                    break;
                }
            }        
            
            for(gyb=y;gyb;gyb--)
            {
                if(obuf[pvx-(y-gyb)*W->W]!=ov)
                {
                    gb = obuf[pvx-(y-gyb)*W->W] - ov;
                    break;
                }
            }
            for(gyu=y;gyu<W->H;gyu++)
            {
                if(obuf[pvx+(gyu-y)*W->W]!=ov)
                {
                    gu = obuf[pvx+(gyu-y)*W->W] - ov;
                    break;
                }
            }      
            
            //if((gxr-gxl)<(gyu-gyb))
            {


                gl = gl?gl:gr;
                gr = gr?gr:gl;


                if(gl>0 && gr>0)
                {
                    //255 ------- 0

                    pbuf[pbx++] = 255-255*(x-gxl)/(gxr-gxl);
                    pbuf[pbx++] = 255-255*(x-gxl)/(gxr-gxl);
                    pbuf[pbx++] = 255-255*(x-gxl)/(gxr-gxl);
                }
                else if(gl<0 && gr<0)
                {
                    //0 ----- 255
                    pbuf[pbx++] = 255*(x-gxl)/(gxr-gxl);
                    pbuf[pbx++] = 255*(x-gxl)/(gxr-gxl);
                    pbuf[pbx++] = 255*(x-gxl)/(gxr-gxl);

                }
                else if(gl>0 && gr<0)
                {   
                    //255---0----255
                    pbuf[pbx++] = 2*abs((int)(255*(x-gxl)/(gxr-gxl)-128));  
                    pbuf[pbx++] = 2*abs((int)(255*(x-gxl)/(gxr-gxl)-128));  
                    pbuf[pbx++] = 2*abs((int)(255*(x-gxl)/(gxr-gxl)-128));  

                }
                else if(gl<0 && gr>0)
                {   
                    //0---255----0
                    pbuf[pbx++] = 255-2*abs((int)(255*(x-gxl)/(gxr-gxl)-128));  
                    pbuf[pbx++] = 255-2*abs((int)(255*(x-gxl)/(gxr-gxl)-128));  
                    pbuf[pbx++] = 255-2*abs((int)(255*(x-gxl)/(gxr-gxl)-128));  

                }

                else
                {
                    //printf("gl:%d gr:%d\n",gl,gr);
                    pbuf[pbx++] = 255;
                    pbuf[pbx++] = 0;
                    pbuf[pbx++] = 0;
                }
            }
            /*
            else
            {

                if(gu>0 && gb>0)
                {
                    //255 ------- 0

                    pbuf[pbx++] = 255-255*(y-gyb)/(gyu-gyb);
                    pbuf[pbx++] = 255-255*(y-gyb)/(gyu-gyb);
                    pbuf[pbx++] = 255-255*(y-gyb)/(gyu-gyb);
                }
                else if(gu<0 && gb<0)
                {
                    pbuf[pbx++] = 255*(y-gyb)/(gyu-gyb);
                    pbuf[pbx++] = 255*(y-gyb)/(gyu-gyb);
                    pbuf[pbx++] = 255*(y-gyb)/(gyu-gyb);

                }
                else
                {
                    pbuf[pbx++] = 0;
                    pbuf[pbx++] = 255;
                    pbuf[pbx++] = 0;
                }
            }
            */
            pvx++;

            //else{
            //pbuf[pbx++] = (Window->colormap[oval] & 0x0000FF) >>16;
            //pbuf[pbx++] = (Window->colormap[oval] & 0x00FF00) >> 16;
            //pbuf[pbx++] = (Window->colormap[oval] & 0xFF0000) >> 16;
            //}



        }
    }

    delete T;
}
void frameBuffer_setWindowPosition(double cx, double cy, int zoomFactor)
{
    // prepare drawing
    Window->xc = cx;
    Window->yc = cy;
    Window->zoom = zoomFactor;
    startWorkload(Window);
}
double getWindowPositionX()
{
    return Window->xc;
}

double getWindowPositionY()
{
    return Window->yc;
}

void frameBuffer_pan(int dx, int dy)
{

    double ddx;
    double ddy;

    uint32_t nextbufid = (current_buf_id + 1) % NUMBUFS;

    // memcpy(outputBuf[nextbufid], outputBuf[current_buf_id],out_buf_size);

    Window->outputBuf = outputBuf[nextbufid];
    Window->ppeBuf = PIXBUFF[nextbufid];

    uint32_t wlen = Window->xend - Window->xstart;
    uint32_t whgh = Window->yend - Window->ystart;

    uint32_t pbufwlen = wlen * 3 * sizeof(guchar);
    uint32_t obufwlen = wlen * sizeof(uint32_t);
    uint32_t pbufwhgh = whgh * 3 * sizeof(guchar);

    WORKLOAD *SubWindow = (WORKLOAD *)malloc(sizeof(WORKLOAD));

    if (dx)
    {
        Window->xc += pow(2, Window->zoom) * (double)dx / (double)Window->W;
        memcpy(SubWindow, Window, sizeof(WORKLOAD));
        if (dx > 0)
        {

            for (unsigned int y = Window->ystart; y < Window->yend; y++)
            {
                memcpy((void *)((size_t)PIXBUFF[nextbufid] + y * pbufwlen), (void *)((size_t)PIXBUFF[current_buf_id] + y * pbufwlen + dx * 3), pbufwlen - dx * 3);
                memcpy((void *)((size_t)outputBuf[nextbufid] + y * obufwlen), (void *)((size_t)outputBuf[current_buf_id] + y * obufwlen + dx * 4), obufwlen - dx * 4);
            }

            SubWindow->xstart = Window->xend - dx;
            SubWindow->xend = Window->xend;

            startWorkload(SubWindow);
        }
        else
        {
            dx = -dx;
            for (unsigned int y = Window->ystart; y < Window->yend; y++)
            {
                memcpy((void *)((size_t)PIXBUFF[nextbufid] + y * pbufwlen + dx * 3), (void *)((size_t)PIXBUFF[current_buf_id] + y * pbufwlen), pbufwlen - dx * 3);
                memcpy((void *)((size_t)outputBuf[nextbufid] + y * obufwlen + dx * 4), (void *)((size_t)outputBuf[current_buf_id] + y * obufwlen), obufwlen - dx * 4);
            }
            // memcpy((void*)((size_t)PIXBUFF[nextbufid]+dx*3), PIXBUFF[current_buf_id], pix_buf_size-dx*3);

            SubWindow->xstart = 0;
            SubWindow->xend = dx;

            startWorkload(SubWindow);
        }
    }

    if (dy)
    {
        Window->yc += pow(2, Window->zoom) * dy / (float)Window->H;
        memcpy(SubWindow, Window, sizeof(WORKLOAD));

        if (dy > 0)
        {
            // memcpy(outputBuf[nextbufid], (void*)((size_t)outputBuf[current_buf_id]+dy*wlen),out_buf_size-dy*wlen);
            memcpy(PIXBUFF[nextbufid], (void *)((size_t)PIXBUFF[current_buf_id] + dy * pbufwlen), pix_buf_size - dy * pbufwlen);
            memcpy(outputBuf[nextbufid], (void *)((size_t)outputBuf[current_buf_id] + dy * obufwlen), out_buf_size - dy * obufwlen);

            SubWindow->ystart = Window->yend - dy;
            SubWindow->yend = Window->yend;
            // SubWindow->xstart = Window->xstart;
            // SubWindow->xend = Window->xend;
            startWorkload(SubWindow);
        }
        else
        {
            dy = -dy;
            // memcpy((void*)((size_t)outputBuf[nextbufid]+dy*wlen),outputBuf[current_buf_id],out_buf_size-dy*wlen);
            memcpy((void *)((size_t)PIXBUFF[nextbufid] + dy * pbufwlen), PIXBUFF[current_buf_id], pix_buf_size - dy * pbufwlen);
            memcpy((void *)((size_t)outputBuf[nextbufid] + dy * obufwlen), outputBuf[current_buf_id], out_buf_size - dy * obufwlen);

            SubWindow->ystart = 0;
            SubWindow->yend = dy;
            startWorkload(SubWindow);
        }
    }
    current_buf_id = nextbufid;
}

void generate_gradient_map(){
     
    /*
        metot 1:
        
        1. X1.......X.........X2  0 .......X......255 / 255.....X.....0 / 0 .....255......0 / 255........0........255
        2. ..........X........X2  0........X......255 / 255.....X.....0
        3. X1......X...........   0........X......255 / 255.....X.....0
        4. .........X..........   0........255.....0


        metot 2:
            onceki buffer [out]
            onceki cx,cy,cz

        sonrakini hesaplarken;
            - yeni cx,cy,cz
            - 
    */
}

void frameBuffer_zoom(int dzoom)
{
    Window->zoom += dzoom;
    startWorkload(Window);
}

void set_col_map(uint32_t *colmap)
{
}

void set_key_cols(int num_key_cols, double *X, uint32_t *keycols)
{
    // todo: create colormap
    uint8_t r0, r1, r;
    uint8_t g0, g1, g;
    uint8_t b0, b1, b;

    for (int x = 0; x < Window->numlvl; x++)
    {
        double xr=(1.0 * x / (Window->numlvl-1));
        if (xr <= X[0])
        {
           // printf("@\n");
            Window->colormap[x] = keycols[0];
        }
        else if (xr >= X[num_key_cols - 1])
        {
            //printf("@@\n");
            Window->colormap[x] = keycols[num_key_cols - 1];
        }
        else
        {
           // printf("@@@\n");
            for (int ki = 1; ki < num_key_cols; ki++)
            {
                if ((xr < X[ki]) && (xr >= X[ki - 1]))
                    {
                        double scl = (xr - X[ki - 1]) / (X[ki] - X[ki - 1]);
                        r0 = (keycols[ki - 1] & 0x0000FF);
                        r1 = (keycols[ki] & 0x0000FF);

                        g0 = (keycols[ki - 1] & 0x00FF00) >> 8;
                        g1 = (keycols[ki] & 0x00FF00) >> 8;

                        b0 = (keycols[ki - 1] & 0xFF0000) >> 16;
                        b1 = (keycols[ki] & 0xFF0000) >> 16;

                        r = r0 + (r1 - r0) * scl;
                        g = g0 + (g1 - g0) * scl;
                        b = b0 + (b1 - b0) * scl;

                        Window->colormap[x] = (r | (g << 8) | (b << 16));
                        break;
                    }
            }
        }
        r = Window->colormap[x] & 0xFF;
        g = (Window->colormap[x] & 0xFF00)>>8;
        b = (Window->colormap[x] & 0xFF0000)>>16;


       // printf("keycol %d : r:%d g:%d b:%d\n", x,r,g,b);
    }
}

void save_state(){
    //save window
    FILE *file = fopen("framebuffer.save", "wb");
    
    
    fwrite(Window,1, sizeof(WORKLOAD), file);

    fwrite(Window->colormap, 1,sizeof(uint32_t)*Window->numlvl, file);

    printf("save Window->zoom: %d\n",Window->zoom);
    printf("save Window->x: %Lf\n",Window->xc);
    printf("save Window->y: %Lf\n",Window->yc);

    fclose(file);
}
void load_state(){

    FILE *file = fopen("framebuffer.save", "rb");
    
    WORKLOAD Wbk;
    //printf("1load Window->zoom: %d\n",Window->zoom);
    //printf("2load Window->x: %Lf\n",Window->xc);
    //printf("3load Window->y: %Lf\n",Window->yc);
    memcpy(&Wbk,Window,sizeof(WORKLOAD));
    //printf("4load Window->zoom: %d\n",Window->zoom);
    //printf("5load Window->x: %Lf\n",Window->xc);
    //printf("6load Window->y: %Lf\n",Window->yc);
    if(file !=NULL)
    {

        fread(Window, 1,sizeof(WORKLOAD), file);
        //printf("7load Window->zoom: %d\n",Window->zoom);
        //printf("load Window->zoom: %d\n",Window->zoom);
        //printf("load Window->x: %Lf\n",Window->xc);
        //printf("load Window->y: %Lf\n",Window->yc);

        

        //reload buffers
        Window->outputBuf = Wbk.outputBuf;
        Window->ppeBuf = Wbk.ppeBuf;
        Window->histogram = Wbk.histogram;
        Window->callback = Wbk.callback;
        Window->colormap = Wbk.colormap;

        //reload colormap
        fread(Window->colormap, 1,sizeof(uint32_t)*Window->numlvl, file);

        fclose(file);
        
    }
    startWorkload(Window);
}
void rand_key_cols(int num_key_cols){

    double*X=new double[num_key_cols];
    uint32_t*rgb = new uint32_t[num_key_cols];
    
    for(int i=0;i<num_key_cols;i++)
    {
        X[i] = 1.0*i/(num_key_cols-1);
        
        uint8_t r,g,b;

        r = 255.0*rand()/RAND_MAX;
        g = 255.0*rand()/RAND_MAX;
        b = 255.0*rand()/RAND_MAX;
        rgb[i] = (r | (g<<8) | (b<<16));

     //   printf("X: %0.3f r:%d g:%d b:%d\n",X[i],r,g,b);
    }

    set_key_cols(num_key_cols, X,rgb);
    startWorkload(Window);
    delete X;
    delete rgb;

}

void toggle_hist_eq(){
    Window->hist_eq=!Window->hist_eq;
    printf("Window->hist_eq %s\n",Window->hist_eq?"true":"false");
    startWorkload(Window);
    
}

void toggle_histogram_stretch(){
    Window->hist_stretch=!Window->hist_stretch;
    printf("Window->hist_stretch %s\n",Window->hist_stretch?"true":"false");
    startWorkload(Window);
    
}

void toggle_show_transitions(){
    Window->show_trans=!Window->show_trans;
    printf("Window->show_trans %s\n",Window->show_trans?"true":"false");
    startWorkload(Window);
    
}

void toggle_color_gradient(){
    Window->color_gradient=!Window->color_gradient;
    printf("Window->color_gradient %s\n",Window->color_gradient?"true":"false");
    startWorkload(Window);
    
}

void inc_bailout(){Window->bailout++; printf("b%d\n",Window->bailout); startWorkload(Window);}
void dec_bailout(){Window->bailout--; printf("b%d\n",Window->bailout); startWorkload(Window);}

frameBuffer::frameBuffer()
{
}
