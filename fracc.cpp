#include <gtk/gtk.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <iomanip>      // std::setprecision
#include "framebuffer.h"
#include <iostream>
#include <vector>
#include "menu.h"
#include "colorMapEditor.h"
//#include <boost/multiprecision/cpp_bin_float.hpp>
//#include <boost/math/special_functions/gamma.hpp>

#define IMGW (512)
#define IMGH (512)

#define XPAD (0 * IMGW / 8)
#define YPAD (0 * IMGH / 8)

#define FRACTALX (IMGW + XPAD * 2)
#define FRACTALY (IMGH + YPAD * 2)

#define MANDELBROT_MAXITR 8
// #define NORMALIZE
// guchar pixbuf[IMGW*IMGH* 3];
guchar *pixbuf;

guint maxVal;
guint minVal;

guint fractalVals[FRACTALX * FRACTALY];

GdkPixbuf *pb;
GtkWidget *image;
//GtkWidget *area;

// shifting:
// guchar tonemap[MANDELBROT_MAXITR][3];
guchar tonemaptype = 0;
guchar normalizeEnabled = 0;
GtkWidget*drawArea;
GtkWidget *window;
GtkWidget*commandBox;
GtkWidget *entry;
typedef struct locationp_
{

    double xcenter;
    double ycenter;
    int zoom;
    double zoomfactor;
  //  guchar tonemap[MANDELBROT_MAXITR][3];

} locationp;

#define DEFX -0.8
#define DEFY 0
#define DEFZOOM 2
#define DEFZOOMFACTOR 1.2

locationp pos = {DEFX, DEFY, DEFZOOM, DEFZOOMFACTOR};


//using namespace boost::multiprecision;

#define NUMKEYCOLS 8
//(1 + (MANDELBROT_MAXITR / 8))
void createToneMap()
{
    int i;

    guchar keycols[NUMKEYCOLS][3];

    for (i = 0; i < NUMKEYCOLS; i++)
    {

        if (i < NUMKEYCOLS - 1)
        {
            keycols[i][0] = rand() & 255;
            keycols[i][1] = rand() & 255;
            keycols[i][2] = rand() & 255;
        }
        else
        {
            keycols[i][0] = 0;
            keycols[i][1] = 0;
            keycols[i][2] = 0;
        }
        // printf("keycol %d= [%d %d %d]\n",i,keycols[i][0],keycols[i][1],keycols[i][2]);
    }
    /*
={
    {0,0,0},
    {0,255,0},
    {255,0,0},
    {0,0,255},
    {255,0,255},
    {0,0,255},
    {255,0,0},
    {0,255,0},
    {0,0,0}
  };
*/

    /*
  guchar keycols[][3]={
    {255,255,255},
    {0,0,0}
  };
  */

    int lenkeycols = sizeof(keycols) / 3;

    int keycolsteplen = MANDELBROT_MAXITR / (lenkeycols - 1);
    printf("keycolsteplen=%d\n", keycolsteplen);

    int kx = 0;
    int keycolindex = 0;

    guchar startcol[3];
    guchar endcol[3];

    for (i = 0; i < MANDELBROT_MAXITR; i++)
    {

        if ((keycolsteplen - 1) <= kx)
        {
            kx = 0;
            keycolindex++;
        }
        else
            kx++;

        startcol[0] = keycols[keycolindex][0];
        startcol[1] = keycols[keycolindex][1];
        startcol[2] = keycols[keycolindex][2];

        endcol[0] = keycols[keycolindex + 1][0];
        endcol[1] = keycols[keycolindex + 1][1];
        endcol[2] = keycols[keycolindex + 1][2];

     //   pos.tonemap[i][0] = startcol[0] + (kx * (endcol[0] - startcol[0])) / keycolsteplen;
     //   pos.tonemap[i][1] = startcol[1] + (kx * (endcol[1] - startcol[1])) / keycolsteplen;
     //   pos.tonemap[i][2] = startcol[2] + (kx * (endcol[2] - startcol[2])) / keycolsteplen;

        // printf("keycolindex=%d kx=%d endcol=%d %d %d tone=%d %d %d\n",keycolindex,kx,endcol[0],endcol[1],endcol[2],tonemap[i][0],tonemap[i][1],tonemap[i][2]  );
    }

    /*
  for(i=0;i<lenkeycols-1;i++){
       startcol[0]=keycols[i][0];
       startcol[1]=keycols[i][1];
       startcol[2]=keycols[i][2];

       endcol[0]=keycols[i+1][0];
       endcol[1]=keycols[i+1][1];
       endcol[2]=keycols[i+1][2];



       kx=0;
       while(kx<keycolsteplen)
       {

           tonemap[kx+(i*keycolsteplen)][0]=startcol[0]+(kx*(endcol[0]-startcol[0]))/keycolsteplen;
           tonemap[kx+(i*keycolsteplen)][1]=startcol[1]+(kx*(endcol[1]-startcol[1]))/keycolsteplen;
           tonemap[kx+(i*keycolsteplen)][2]=startcol[2]+(kx*(endcol[2]-startcol[2]))/keycolsteplen;
           kx++;
       }
  }
*/

    for (i = 0; i < MANDELBROT_MAXITR; i++)
    {
        //    printf("tonemap[%d]=[%d %d %d]\n",i,tonemap[i][0],tonemap[i][1],tonemap[i][2]);
    }
}



void pan(int px, int py)
{
    // pan by pixel value.
    gint x, y;
    gint xpan, ypan;
    gint panindex, index;
    guint *panimg = (guint *)malloc(FRACTALX * FRACTALY * sizeof(guint));

    panindex = 0;

    for (y = 0; y < FRACTALY; y++)
    {
        for (x = 0; x < FRACTALX; x++)
        {
            xpan = x - px;
            ypan = y - py;

            if (xpan >= 0 && xpan < FRACTALX && ypan >= 0 && ypan < FRACTALY)
            {
                index = ypan * FRACTALX + xpan;
                panimg[panindex] = fractalVals[index];
            }
            else
            {
                panimg[panindex] = 1000;
                // printf("CALC %d %d\n", x,y);
            }
            panindex++;
        }
    }

    double ar = FRACTALX / FRACTALY;

    pos.xcenter = pos.xcenter - (int)px * 2 * powf(pos.zoomfactor, pos.zoom) / (FRACTALX - 1 - (2 * XPAD));
    pos.ycenter = pos.ycenter - (int)py * 2 * powf(pos.zoomfactor, pos.zoom) / (FRACTALY - 1 - (2 * YPAD));

    //    printf("xcenter=%0.12f ycenter=%0.12f\n",pos.xcenter, pos.ycenter );
    // xmin=pos.xcenter-powf(pos.zoomfactor,pos.zoom)*ar;

    memcpy(fractalVals, panimg, FRACTALX * FRACTALY * sizeof(guint));
    free(panimg);
}

guint mandelbrot0(double x, double y)
{

    double xn, yn;
    double rsq = 0.0;
    double isq = 0.0;
    double zsq = 0.0;
    guint itr = 0;
    double xna, yna;

    xn = 0;
    yn = 0;

    while ((rsq + isq) <= 4 && itr < (MANDELBROT_MAXITR - 1))
    {

        // xn=rsq - isq +x;
        // y=zsquare - rsquare - isquare + yy;     //2xy+yy
        // yn=zsq+y;

        xna = xn * xn - yn * yn + xn + x;
        yna = 2 * xn * yn + yn + y;

        xn = xna;
        yn = yna;
        rsq = xn * xn;
        isq = yn * yn;

        // zsquare=(x+y)*(x+y);//r^2 + i^2 +2xy
        // zsq=2*xn*yn;

        // steps[iteration]=(rsquare + isquare);
        itr++;
    }

    return itr;
}

guint mandelbrot(double x, double y)
{

    double xn, yn;
    double rsq = 0.0;
    double isq = 0.0;
    double zsq = 0.0;
    guint itr = 0;

    while ((rsq + isq) <= 4 && itr < (MANDELBROT_MAXITR - 1))
    {

        xn = rsq - isq + x;
        // y=zsquare - rsquare - isquare + yy;     //2xy+yy
        yn = zsq + y;

        rsq = xn * xn;
        isq = yn * yn;

        // zsquare=(x+y)*(x+y);//r^2 + i^2 +2xy
        zsq = 2 * xn * yn;

        // steps[iteration]=(rsquare + isquare);
        itr++;
    }

    return itr;
}

guint fractalCalcVar()
{

    guint x, y;
    guint index = 0, indexrgb = 0;
    guint tone;
    gulong meanval;
    guint numdifftones;
    meanval = 0;

    // calculate the number of different tones
    guchar *tones = (guchar *)g_malloc(MANDELBROT_MAXITR);
    memset(tones, 0, MANDELBROT_MAXITR);

    numdifftones = 0;
    for (y = 0; y < IMGH; y++)
    {
        index = (YPAD + y) * FRACTALX + XPAD;
        for (x = 0; x < IMGW; x++)
        {

            tone = fractalVals[index];

            if (tones[tone] == 0)
            {
                tones[tone] = 1;
                numdifftones++;
            }

            if (tone < minVal)
                minVal = tone;

            if (tone > maxVal)
                maxVal = tone;

            meanval += tone;
            index++;
        }
    }

    meanval = meanval / index;

    if (minVal == maxVal)
        return 0;

    if (numdifftones < 16)
        return 0;

    return 1;
}

void mandelbrotDraw(gboolean recalc)
{

    gint x, y;
    double xreal, yreal;

    double xmin, xmax, ymin, ymax;
    double ar = 1.0 * FRACTALX / FRACTALY;

    guint val;

    xmin = pos.xcenter - powf(pos.zoomfactor, pos.zoom) * ar;
    xmax = pos.xcenter + powf(pos.zoomfactor, pos.zoom) * ar;

    ymin = pos.ycenter - powf(pos.zoomfactor, pos.zoom);
    ymax = pos.ycenter + powf(pos.zoomfactor, pos.zoom);

    guint index = 0;

    // printf("zoom=%d xrl=%0.12f %0.12f yrl=%0.12f %0.12f\n",pos.zoom,xmin,xmax, ymin, ymax);

    maxVal = 0;
    minVal = 256;
    guint numcalc = 0;
    double xrealold;

    for (y = 0; y < FRACTALY; y++)
    {

        yreal = ymin + ((ymax - ymin) * ((int)y - YPAD)) / (FRACTALY - (2 * YPAD) - 1);
        // printf("XREAL=%0.12f, y=%d YREAL=%0.12f\n", xreal,y, yreal);

        for (x = 0; x < FRACTALX; x++)
        {

            xreal = xmin + ((xmax - xmin) * (x - XPAD)) / (FRACTALX - 1 - (2 * XPAD));

            // printf("xdiff=%0.12f %0.12f\n", xreal-xrealold, 2*powf(pos.zoomfactor,pos.zoom)/(FRACTALX-1-2*XPAD));
            // yreal=ymin+((ymax-ymin)*y)/(FRACTALY-1);
            xrealold = xreal;

            if (!recalc && fractalVals[index] != 1000)
            {

                index++;
                continue;
            }

            // if(!recalc){
            // if(y==0)
            //     printf("calc x=%d y=%d xreal=%0.12f yreal=%0.12f\n",x,y, xreal, yreal);
            // fractalVals[index]=128;
            // index++;
            // continue;
            // }

            numcalc++;

            val = mandelbrot(xreal, yreal);
            // val=255;
            fractalVals[index] = val;

            if (maxVal < val)
                maxVal = val;

            if (minVal > val)
                minVal = val;

            index++;
        }
    }

    gtk_widget_queue_draw(drawArea);

    // save xcenter ycenter zoom.

    FILE *file = fopen("fractalpos.save", "wb");
    fwrite(&pos, sizeof(pos), 1, file);
    fclose(file);
}

void saveImage(gchar *name, gchar *type)
{

    GdkPixbuf *pb = gdk_pixbuf_new_from_data(pixbuf, GDK_COLORSPACE_RGB, FALSE, 8, IMGW, IMGH, IMGH * 3, NULL, 0);

    printf("saving %s.%s pb=%u\n", name, type, pb);
    // printf("%s\n",type);
    gdk_pixbuf_save(pb, name, type, NULL);
}

std::string doCommand(std::string cstr){
  

    std::ostringstream response;


    if(cstr=="help")
    {
        response<<"no help!";
    }
    else if(cstr=="x")
    {
        response<<std::setprecision(12)<<getWindowPositionX();
    }
    else if(cstr=="y")
    {
        response<<std::setprecision(12)<<getWindowPositionY();
    }
    else
    {
        response <<"unknown command";
    }

    
    return response.str();

}

gboolean my_keypress_function_w2(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
    //printf("key press:0x%08x \n",commandBox);

    

    if (event->keyval == GDK_KEY_Return)
    {
        GtkTextBuffer *buffer;
        const gchar*command=gtk_entry_get_text(GTK_ENTRY(entry));
        
        std::string commandstr(command);

        std::string  return_command = doCommand(commandstr);

        buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (commandBox));
       
        gtk_entry_set_text(GTK_ENTRY(entry),"");
        GtkTextIter end;
        gtk_text_buffer_get_end_iter(buffer, &end);
        gtk_text_buffer_insert(buffer, &end, "\n", -1);
        gtk_text_buffer_get_end_iter(buffer, &end);
        commandstr.insert(0,">");
        gtk_text_buffer_insert(buffer, &end, commandstr.c_str(), -1);
        gtk_text_buffer_get_end_iter(buffer, &end);
        gtk_text_buffer_insert(buffer, &end, "\n", -1);
        gtk_text_buffer_get_end_iter(buffer, &end);
        gtk_text_buffer_insert(buffer, &end, return_command.c_str(), -1);
        



        return TRUE;
    }

    return FALSE;
}

gboolean my_keypress_function(GtkWidget *widget, GdkEventKey *event, gpointer data)
{

    

    //printf("key press:0x%08x 0x%08x 0x%08x 0x%08x \n",widget,window, drawArea,gtk_window_get_focus(GTK_WINDOW(window)));

    //if(widget!=drawArea)
    //    return FALSE;

    double ar = IMGW / IMGH;

    if (event->keyval == GDK_KEY_Up)
    {
        // pos.ycenter=pos.ycenter-powf(pos.zoomfactor,pos.zoom)*ar/10;
        frameBuffer_pan(0, -16);
        gtk_widget_queue_draw(drawArea);
        // pan(0,-16);
        // mandelbrotDraw(FALSE);

        return TRUE;
    }
    else if (event->keyval == GDK_KEY_Down)
    {
        // pos.ycenter=pos.ycenter+powf(pos.zoomfactor,pos.zoom)*ar/10;
        // pan(0,16);
        frameBuffer_pan(0, 16);
        gtk_widget_queue_draw(drawArea);
        // mandelbrotDraw(FALSE);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_Left)
    {
        // pos.xcenter=pos.xcenter-powf(pos.zoomfactor,pos.zoom)*ar/10;
        // pan(16,0);
        frameBuffer_pan(-16, 0);
        gtk_widget_queue_draw(drawArea);
        // mandelbrotDraw(FALSE);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_Right)
    {
        // pan(-16,0);
        frameBuffer_pan(16, 0);
        gtk_widget_queue_draw(drawArea);
        // pos.xcenter=pos.xcenter+powf(pos.zoomfactor,pos.zoom)*ar/10;
        // mandelbrotDraw(FALSE);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_z)
    {
        frameBuffer_zoom(-1);
        gtk_widget_queue_draw(drawArea);
        // pos.zoom--;
        // mandelbrotDraw(TRUE);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_x)
    {
        frameBuffer_zoom(1);
        gtk_widget_queue_draw(drawArea);
        // pos.zoom++;
        // mandelbrotDraw(TRUE);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_m)
    {
        toggle_histogram_stretch();
        gtk_widget_queue_draw(drawArea);
    }
    else if (event->keyval == GDK_KEY_s)
    {
        saveImage("img", "png");
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_space)
    {
        printf("space\n");
        rand_key_cols(12);
        gtk_widget_queue_draw(drawArea);

    }
    else if (event->keyval == GDK_KEY_h)
    {
        toggle_hist_eq();
        gtk_widget_queue_draw(drawArea);
    }
    else if (event->keyval == GDK_KEY_b)
    {
        printf("b\n");
        dec_bailout();
        gtk_widget_queue_draw(drawArea);
    }
    else if (event->keyval == GDK_KEY_B)
    {
        printf("B\n");
        inc_bailout();
        gtk_widget_queue_draw(drawArea);
    }
    else if(event->keyval == GDK_KEY_t)
    {
        toggle_show_transitions();
        gtk_widget_queue_draw(drawArea);
    }
    else if(event->keyval == GDK_KEY_g)
    {
        toggle_color_gradient();
        gtk_widget_queue_draw(drawArea);
    }
    else if (event->keyval == GDK_KEY_r)
    {
        pos.xcenter = -2.0 + 4.0 * rand() / (1.0 * RAND_MAX);
        pos.ycenter = -2.0 + 4.0 * rand() / (1.0 * RAND_MAX);
        pos.zoom = -10 - (rand() % 10);
        while (1)
        {
            printf("x=%f, y=%f, zoom=%d\n", pos.xcenter, pos.ycenter, pos.zoom);
            mandelbrotDraw(TRUE);
            if (fractalCalcVar())
                break;
            pos.xcenter = -2.0 + 4.0 * rand() / (1.0 * RAND_MAX);
            pos.ycenter = -2.0 + 4.0 * rand() / (1.0 * RAND_MAX);

            pos.zoomfactor = DEFZOOMFACTOR;
            pos.zoom = -10 - (rand() % 10);
        }
    }
    else if (event->keyval == GDK_KEY_0)
    {

        pos.xcenter = DEFX;
        pos.ycenter = DEFY;
        pos.zoom = DEFZOOM;
        pos.zoomfactor = DEFZOOMFACTOR;
        mandelbrotDraw(TRUE);
        return TRUE;
    }


    return FALSE;
}

void fillRandom(guchar *arr, guint len)
{

    while (len--)
        arr[len] = rand() & 0xFF;
}

int function()
{
    gulong unixTime;

    char imgName[64];
    char posName[64];
    printf("timer\n");

    // pos.xcenter=-2.0+4.0*rand()/(1.0*RAND_MAX);
    // pos.ycenter=-2.0+4.0*rand()/(1.0*RAND_MAX);

    // pos.xcenter = pos.xcenter  -0.005 + rand()/(100.0*RAND_MAX);
    // pos.ycenter = pos.ycenter  -0.005 + rand()/(100.0*RAND_MAX);

    // pos.zoom= -20-(rand()%20);
    while (1)
    {

        pos.xcenter = -2.0 + 4.0 * rand() / (1.0 * RAND_MAX);
        pos.ycenter = -2.0 + 4.0 * rand() / (1.0 * RAND_MAX);

        pos.xcenter = pos.xcenter - 0.00005 + 0.0001 * rand() / (RAND_MAX);
        pos.ycenter = pos.ycenter - 0.00005 + 0.0001 * rand() / (RAND_MAX);

        pos.zoom = -20 - (rand() % 20);

        pos.zoomfactor = DEFZOOMFACTOR;

        printf("x=%f, y=%f, zoom=%d\n", pos.xcenter, pos.ycenter, pos.zoom);
        mandelbrotDraw(TRUE);
        if (fractalCalcVar())
            break;
    }

    createToneMap();

    mandelbrotDraw(TRUE);
    unixTime = g_date_time_to_unix(g_date_time_new_now_local());
    sprintf(imgName, "%u.png", unixTime);
    sprintf(posName, "%u.pos", unixTime);

    saveImage(imgName, "png");

    FILE *file = fopen(posName, "wb");
    fwrite(&pos, sizeof(pos), 1, file);
    fclose(file);

    g_timeout_add(1000, (GSourceFunc)function, 0);

    return 0;
}


static gboolean
your_draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    printf("your_draw_cb 0x%08x\n", cr);
    // printf("widget 0x%08x\n", widget);

    GdkPixbuf *pb = gdk_pixbuf_new_from_data(getOutBuf(), GDK_COLORSPACE_RGB, FALSE, 8, IMGW, IMGH, 3 * (IMGW + 2 * XPAD), NULL, 0);

    gdk_cairo_set_source_pixbuf(cr, pb, 0, 0);

    cairo_paint(cr);
    // cairo_destroy (cr);

    save_state();
    return FALSE;
}



static void exportImage(GtkWidget *widget,
                        gpointer data)
{
    // g_print ("Hello again - %s was pressed\n", (gchar *) data);

    //
    gchar *S = g_date_time_format(g_date_time_new_now_local(), "%Y%m%d%k%M%S");
    // printf("%s>\n",S);
    saveImage(S, "png");
    g_free(S);
}

static void menu_slot(GtkMenuItem *menuitem, gpointer data)
{
    printf("%s\n", data);
}


void tool_box()
{
    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Image");
    gtk_window_set_default_size(GTK_WINDOW(window), 200, 400);
    // gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_LEFT);
    gtk_widget_show_all(window);
}

int main(int argc,
         char **argv)
{

    
    GtkApplication *app;
    int status;

    printf("size of float=%d\n", sizeof(float));
    printf("size of double=%d\n", sizeof(double));
    printf("size of long double=%d\n", sizeof(long double));

 

    /*
    cpp_bin_float_100 y=0.0000000000000000000033881317890172013562732900027185678482055664062500000000000000000000000000000000;
    cpp_bin_float_100 x=0.7960937500000000452762827229946651641512289643287658691406250000000000000000000000000000000000000000;

    cpp_bin_float_100 z = x+y;
    cpp_bin_float_100 t = z-y;
    std::cout << "x:"<<x.str(100)<<std::endl;
    std::cout << "y:"<<y.str(100)<<std::endl;
    std::cout << "z:"<<z.str(100)<<std::endl;
    std::cout << "t:"<<t.str(100)<<std::endl;
    */
   // return 0;
    pixbuf = createFrameBuffer(IMGW, IMGH, XPAD, YPAD);
    
    //frameBuffer_setWindowPosition(DEFX, DEFY, DEFZOOM);

    //
    

    int i;
    gtk_init(&argc, &argv);

    // GtkWidget *textview,*textview2;
    // GtkTextBuffer *buffer,*buffer2;

    // textview = gtk_text_view_new ();
    // textview2 = gtk_text_view_new ();

    // buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
    // buffer2 = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview2));

    // gtk_text_buffer_set_text (buffer, "Hello, this is some text", -1);
    // gtk_text_buffer_set_text (buffer, "Hello, this is some 2", -1);

    GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Image");
    gtk_window_set_default_size(GTK_WINDOW(window), IMGW, IMGH + 80);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

    GtkWidget *menu = createMenu((void*)menu_slot);

    gtk_box_pack_start(GTK_BOX(vbox), menu, FALSE, FALSE, 0);

#if 1
    drawArea = gtk_drawing_area_new();
    g_signal_connect(G_OBJECT(drawArea), "draw", G_CALLBACK(your_draw_cb), NULL);

    // gtk_container_add(GTK_CONTAINER(frame), area);

    gtk_widget_set_size_request(drawArea, IMGW, IMGH);
    // gtk_widget_set_size_request(textview, IMGW, 32);

    gtk_box_pack_start(GTK_BOX(vbox), drawArea, FALSE, FALSE, 0);
    // gtk_box_pack_start (GTK_BOX (vbox), textview, FALSE, FALSE, 0);

    
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);

    g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK(my_keypress_function), NULL);

    //  GtkWidget*frame=gtk_frame_new("lbl");
    //  gtk_widget_set_size_request(frame,256,256);
    //  gtk_container_add (GTK_CONTAINER (window), frame);
    //gtk_widget_set_focus_on_click(drawArea, TRUE);
#endif

    // g_timeout_add (1000,(GSourceFunc)function,0);

    // tool_box();
    // GtkWidget *entry = gtk_entry_new ();
   // gtk_entry_set_max_length (GTK_ENTRY (entry),0);
   // gtk_table_attach_defaults (GTK_TABLE (table),entry, 0, 1, 0, 1);
    //gtk_widget_show (entry);
   // gtk_box_pack_end(GTK_BOX(vbox), entry, FALSE, FALSE, 0);
    //gtk_container_add(GTK_CONTAINER(vbox), entry);

//   

    gtk_container_add(GTK_CONTAINER(window), vbox);


     gtk_widget_show_all(window);
    // createToneMap();

    // mandelbrotDraw(TRUE);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

#if 0
    GtkWidget*window2=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window2), "Command");
    gtk_window_set_default_size(GTK_WINDOW(window2), 256, 512);
    gtk_window_set_position(GTK_WINDOW(window2), GTK_WIN_POS_CENTER);
    
    GtkWidget *vbox2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    commandBox=gtk_text_view_new();
    gtk_widget_set_size_request(commandBox, 256, 480);
    //gtk_widget_set_focus_on_click(drawArea, FALSE);
    gtk_text_view_set_editable(GTK_TEXT_VIEW(commandBox),FALSE);
    gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(commandBox),FALSE);
    gtk_box_pack_start(GTK_BOX(vbox2), commandBox, FALSE, FALSE, 0);

    gtk_widget_add_events(window2, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window2), "key_press_event", G_CALLBACK(my_keypress_function_w2), NULL);


    
    entry = gtk_entry_new ();
    gtk_entry_set_max_length (GTK_ENTRY (entry),0);
    gtk_box_pack_end(GTK_BOX(vbox2), entry, FALSE, FALSE, 0);
    gtk_widget_grab_focus(entry);
    
    gtk_container_add(GTK_CONTAINER(window2), vbox2);
   
    gtk_widget_show_all(window2);


    colorMapEditor*cmaped=new colorMapEditor();
#endif

   load_state();
    gtk_main();

    return 0;
}
