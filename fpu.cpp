//fractal processing units
#include "fpu.h"
#include "stdio.h"
#include <thread>         // std::thread
#include "math.h"
//#include <boost/multiprecision/cpp_bin_float.hpp>
//#include <boost/math/special_functions/gamma.hpp>
//#include <boost/multiprecision/cpp_complex.hpp>

//namespace bmp = boost::multiprecision;
//using Complex = bmp::cpp_complex_100;
//using Real    = Complex::value_type;

//using namespace boost::multiprecision;

#define NUMTHREADS  16

#define HPREC_FLOAT long double
//#define MANDELBROT_MAXITR 256

/*
        X0   = x
        Y0   = y
        Z0   = X+jY
        Z    = x^2 - y^2 + 2jxy + x + jy
             = x*(x+jy+1) - y*(y+jy+1)

        x0 = x+xd
        Y0 = j(y+yd)
        Z = x^2+2x^2d+x^2d^2+

*/
std::thread threads[NUMTHREADS];

static uint32_t mandelbrot(HPREC_FLOAT x, HPREC_FLOAT y, int bailout, uint32_t MANDELBROT_MAXITR)
{

    HPREC_FLOAT xn, yn;
    HPREC_FLOAT rsq=0.0;
    HPREC_FLOAT isq=0.0;
    HPREC_FLOAT zsq=0.0;
    HPREC_FLOAT rsq_isq = 0.0;
    HPREC_FLOAT rsq_isq_pre=0.0;
    HPREC_FLOAT rsq_isq_post=0.0;

    uint32_t itr=0;

    while (rsq_isq <= bailout && itr < (MANDELBROT_MAXITR-1)){

        xn=rsq - isq +x;
        //y=zsquare - rsquare - isquare + yy;     //2xy+yy
        yn=zsq+y;

        rsq=xn*xn;
        isq=yn*yn;

        //zsquare=(x+y)*(x+y);//r^2 + i^2 +2xy
        zsq=2*xn*yn;

        //steps[iteration]=(rsquare + isquare);
        itr++;

        rsq_isq_pre = rsq_isq;
        rsq_isq = rsq + isq;

    }


    if(itr==(MANDELBROT_MAXITR-1))
    {
        return itr;
    }
    else
    {
        if(rsq_isq>4.0){
            uint8_t ratio = int((255.0*(rsq_isq - bailout))/(rsq_isq - rsq_isq_pre)) ;
            return  (ratio<<16) | itr;    
        }
        else
        {
            return itr;
        }
    }


    

}


void wloadThread(WORKLOAD*W){

    //printf("worker thread Y = %d -- %d X=%d -- %d\n",W->ystart, W->yend, W->xstart, W->xend);
    //printf("W=%d H=%d\n", W->W, W->H);
    //printf("XC:%0.3f YC=%0.3f\n",W->xc, W->yc );
    //printf("ZOOM=%d\n", W->zoom);
    //printf("obuf=0x%08x\n",W->outputBuf);

    HPREC_FLOAT x,y;

    uint32_t xi,yi;

    HPREC_FLOAT height=pow(2,W->zoom);
    HPREC_FLOAT width=pow(2,W->zoom);

    HPREC_FLOAT ar=1.0*W->W/W->H;

    HPREC_FLOAT ybtm = (HPREC_FLOAT)W->yc  - (HPREC_FLOAT)(height/2.0);
    HPREC_FLOAT xleft = (HPREC_FLOAT)W->xc - ar*(HPREC_FLOAT)(width/2.0);

    HPREC_FLOAT ystep = height/(HPREC_FLOAT)W->H;
    HPREC_FLOAT xstep = ar*width /(HPREC_FLOAT)(W->W);
    uint32_t outOffset;

    for(yi=W->ystart; yi<W->yend;yi++)
    {
        y = ybtm + ystep*(HPREC_FLOAT)yi;
        outOffset = W->W * yi+W->xstart;

        /*
        if(yi<2)
        {
            printf("y%d: %0.100Lf \n",yi,y);
            printf("ystep: %0.100Lf \n",ystep);
        }
        */
        for(xi=W->xstart;xi<W->xend;xi++)
        {
            x = xleft + xstep*(HPREC_FLOAT)xi;
            
            /*
            if(xi<2 && yi==0)
            {
                printf("x%d: %0.100Lf \n",xi,x);
                printf("xstep: %0.100Lf \n",xstep);
                printf("xleft: %0.100Lf \n",xleft);
            }
            */
            W->outputBuf[outOffset] = mandelbrot(x,y,W->bailout, W->numlvl);
            outOffset++;
        }
    }



}


void startWorkload(WORKLOAD*W){
    printf("xc:%Lf yc:%Lf z:%d\n",W->xc, W->yc,W->zoom);


    int i;
    WORKLOAD*w;

    int winh=W->yend - W->ystart;
    int winw=W->xend - W->xstart;

    if(winh>=winw)
    {
        for(i=0;i<NUMTHREADS;i++)
        {
            w=(WORKLOAD*)malloc(sizeof(WORKLOAD));
            *w=*W;

            w->ystart = W->ystart + (i*winh/NUMTHREADS);
            w->yend = w->ystart + (winh/NUMTHREADS);

            threads[i]=std::thread(wloadThread,w);

        }
    }
    else
    {
        for(i=0;i<NUMTHREADS;i++)
        {
            w=(WORKLOAD*)malloc(sizeof(WORKLOAD));
            *w=*W;

            w->xstart = W->xstart + (i*winw/NUMTHREADS);
            w->xend = w->xstart + (winw/NUMTHREADS);

            threads[i]=std::thread(wloadThread,w);

        }
    }
    for(i=0;i<NUMTHREADS;i++)
    {
        threads[i].join();
    }

    //ppe over whole image
    W->callback(W);

    //std::thread thread (wloadThread,W);     // spawn new thread that calls foo()
    //first.join();
}

fpu::fpu()
{

}
