#ifndef FPU_H
#define FPU_H


#include "job.h"
#include "stdint.h"

typedef struct WORKLOAD{

    
        uint32_t W;
        uint32_t H;

        uint32_t xstart;
        uint32_t xend;
        uint32_t ystart;
        uint32_t yend;
        uint32_t numlvl;
        long double xc;
        long double yc;
        int zoom;



        bool done;
        bool hist_eq;
        bool hist_stretch;
        bool show_trans;
        bool color_gradient;
        int bailout;


    uint32_t*outputBuf;
    void*ppeBuf;
    uint32_t*colormap;
    uint32_t*histogram;
    void (*callback)(WORKLOAD*);


}WORKLOAD;


void startWorkload(WORKLOAD*W);

//JOB*createNewJobElement(unsigned int numElements);
//void addNewJob(JOB*J);
//JOB*getNextJob();
//void jobDone(JOB*J);

/*
 *
 *
 * fpu: calculates the values
 * fpu runs in multi thread
 *  each fpu element asks for workload:
 *      workload: array of jobs
 *          each array is a memory area,
 *              x - y
 *
 * ppe: generates the tones
 * framebuffer: keeps the tone data
 *
 *
 *
 */


//void initWorkerThread(JOB*J);

class fpu
{
public:
    fpu();
};

#endif // FPU_H
