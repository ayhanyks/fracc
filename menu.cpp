#include "menu.h"
#include <string>
#include <vector>

const char *MENU = "\
file\n\
 quit:quit\n\
 save:save\n\
 exportImage:exportImage\n\
image\n\
 resolution:set_resolution\n\
 colorMap\n\
  brownie:colormap_brownie\n\
  reddish:colormap_reddish\n\
help\n\
 about:about\n\
";

GtkWidget *createMenu(void*menu_callback)
{

    GtkWidget *menubar;
    menubar = gtk_menu_bar_new();

    printf("%s\n", MENU);

    // parse MENU
    std::string menustr(MENU);
    // std::vector<std::string>menu_elements;
    typedef struct
    {
        GtkWidget *menu_element;
        int level;
        GtkWidget *subMenu;
    } menuStruct;
    std::vector<menuStruct> parent_menu_list;

    parent_menu_list.push_back((menuStruct){menubar, 0});

    while (1)
    {
        int loc = menustr.find("\n");
        if (loc < 0)
            break;
        std::string s = menustr.substr(0, loc);
        menustr = menustr.substr(loc + 1);
        // menustr = menustr.substr(0)
        // printf("X:%s\n",s.c_str());

        int lvl = -1;
        while (s[++lvl] == ' ')
            ;
        s = s.substr(lvl);

        int colon = s.find(":");
        std::string menu_item_text;
        std::string menu_item_action;
        if (colon >= 0)
        {
            menu_item_text = s.substr(0, colon);
            menu_item_action = s.substr(colon + 1);
        }
        else
        {
            menu_item_text = s;
            menu_item_action = "";
        }

        printf("X:%s lvl:%d menu_item=%s action=%s (%d)\n", s.c_str(), lvl, menu_item_text.c_str(), menu_item_action.c_str(), menu_item_action.length());

        GtkWidget *menuItem = gtk_menu_item_new_with_label(menu_item_text.c_str());

        // printf("parent_menu list len: %d\n", parent_menu_list.size());

        if (lvl == 0)
        {
            gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuItem);
        }
        else
        {
            while (parent_menu_list.back().level >= lvl)
            {
                parent_menu_list.pop_back();
            }

            // printf("parent_menu list len: %d\n", parent_menu_list.size());
            GtkWidget *parent = parent_menu_list.back().menu_element;
            if (gtk_menu_item_get_submenu(GTK_MENU_ITEM(parent)) == NULL)
            {
                gtk_menu_item_set_submenu(GTK_MENU_ITEM(parent), gtk_menu_new());
            }

            gtk_menu_shell_append(GTK_MENU_SHELL(gtk_menu_item_get_submenu(GTK_MENU_ITEM(parent))), menuItem);
        }

        if(menu_item_action!="")
        {
            g_signal_connect(G_OBJECT(menuItem), "activate" ,G_CALLBACK(menu_callback), g_strdup(menu_item_action.c_str()));
        }

        parent_menu_list.push_back((menuStruct){menuItem, lvl});
    }

    return menubar;
}